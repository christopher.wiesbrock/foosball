# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 10:04:40 2024

@author: wiesbrock
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Liste der Dateipfade
file_paths = [
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001\random_forest_all_labels.xlsx",
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001/classification_results.xlsx",
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001/classification_results1.xlsx",
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001/classification_results2.xlsx",
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001/classification_results3.xlsx",
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001/classification_results4.xlsx",
    r"C:\Users\wiesbrock\Downloads\drive-download-20240601T080350Z-001/classification_results5.xlsx"
]

# Initialisiere einen leeren DataFrame für die kombinierten Metriken
combined_metrics_df = pd.DataFrame(columns=['Class', 'F1 Score', 'File'])

# Durchlaufe alle Dateien und lese die F1-Daten ein
for i, file_path in enumerate(file_paths, start=1):
    # Einlesen der Excel-Datei
    df = pd.read_excel(file_path)
    
    # Entfernen der unnötigen Spalte
    
    # Filtern der unerwünschten Klassen (0 und 7-10)
    df = df.iloc[1:-4]
    
    # Extrahiere F1-Daten
    f1_scores = df['f1-score']
    
    # Erstelle DataFrame für die aktuellen Metriken
    metrics_df = pd.DataFrame({
        'Class': df['Label'],  # Annahme: die Spalte 'label' repräsentiert die Klassen
        'F1 Score': f1_scores,
        'File': f'File {i}'
    })
    
    # Füge die aktuellen Metriken dem kombinierten DataFrame hinzu
    combined_metrics_df = pd.concat([combined_metrics_df, metrics_df])
    combined_metrics_df['Class']=combined_metrics_df['Class'].astype(int)

# Plot erstellen
sns.color_palette("viridis", as_cmap=True)
plt.figure(dpi=300,figsize=(12, 8))
sns.swarmplot(x='File', y='F1 Score', hue='Class',palette='viridis',size=7.5, data=combined_metrics_df)
sns.despine()

plt.title('Combined F1 Scores from Multiple Files')
plt.xlabel('File')
plt.ylabel('F1 Score')
plt.ylim(0.2, 0.7)
plt.legend(title='Class')
#plt.grid(True)
plt.show()