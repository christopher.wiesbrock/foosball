# -*- coding: utf-8 -*-
"""
Created on Wed May 22 10:36:14 2024

@author: wiesbrock
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.metrics import accuracy_score, mean_squared_error
from sklearn.multioutput import MultiOutputClassifier
from sklearn.preprocessing import MinMaxScaler

column_names = ['Unnamed: 0','x1', 'x2', 'y1','y2','class', 'conf','frame','label']
data = pd.read_csv(r"C:\Users\wiesbrock\Desktop\Projekte\Foosball csv\concat\concatenated_file.csv", header=1 , names=column_names)
data=data[column_names[1:]]

print('Data loaded')

scaler = MinMaxScaler()
data[['x1', 'x2', 'y1','y2']] = scaler.fit_transform(data[['x1', 'x2', 'y1','y2']])

X = data[['x1', 'x2', 'y1','y2']]
y = data['class']






y = pd.get_dummies(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=1)

print('Dataset splitted')

model = RandomForestClassifier(n_estimators=100, random_state=1)
print('Training started')
model.fit(X_train, y_train)
print('Prediction started')
y_pred = model.predict(X_test)

accuracy = (y_pred == y_test).mean().mean()
print(f'Average Accuracy: {accuracy * 100:.2f}%')

print("Vorhersagen:")
print(y_pred)

# Vorhersagen auf den Testdaten


# Auswertung des Modells
print(classification_report(y_test, y_pred))