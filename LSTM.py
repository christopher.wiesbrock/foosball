# -*- coding: utf-8 -*-
"""
Created on Wed May 22 15:53:23 2024

@author: wiesbrock
"""



import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from sklearn.metrics import classification_report
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense
from tensorflow.keras.utils import to_categorical
from tqdm.keras import TqdmCallback  # Für die Fortschrittsanzeige
from tensorflow.keras.preprocessing.sequence import pad_sequences

# Daten laden und vorbereiten
column_names = ['Unnamed: 0','x1', 'x2', 'y1','y2','class', 'conf','frame','label']
data = pd.read_csv(r"C:\Users\wiesbrock\Desktop\Projekte\Foosball csv\concat\concatenated_file.csv", header=1, names=column_names)
data = data[column_names[1:]]

print('Data loaded')

# Normalisieren der Features
scaler = MinMaxScaler()
data[['x1', 'x2', 'y1', 'y2', 'class']] = scaler.fit_transform(data[['x1', 'x2', 'y1', 'y2', 'class']])

# Zielvariable kodieren
#label_encoder = LabelEncoder()
#data['label'] = label_encoder.fit_transform(data['label'])
#data['label'] = to_categorical(data['label'])

print('Data encoded')
# Daten nach Frame-ID sortieren
data = data.sort_values(by='frame')

# Sequenzen basierend auf Frame-ID erstellen
frames = data['frame'].unique()
X_sequence = []
y_sequence = []

print('Start sequencing')

for frame in frames:
    frame_data = data[data['frame'] == frame]
    X_sequence.append(frame_data[['x1', 'x2', 'y1', 'y2','class']].values)
    y_sequence.append(frame_data['label'].values[0])  # Die erste Klasse im Frame als Zielvariable nehmen

# Konvertieren in numpy arrays
X_sequence = pad_sequences(X_sequence, maxlen=X_sequence[0].shape[0])  # Pad sequences to max length
y_sequence = np.array(y_sequence) 
y_sequence = np.reshape(y_sequence, (y_sequence.shape[0], -1))

# Aufteilen des Datensatzes in Trainings- und Testdaten
X_train, X_test, y_train, y_test = train_test_split(X_sequence, y_sequence, test_size=0.4, random_state=1)

print('Dataset splitted')

# LSTM-Modell definieren
model = Sequential()
model.add(LSTM(50, input_shape=(X_sequence.shape[1], X_sequence.shape[2]), return_sequences=True))
model.add(LSTM(50))
model.add(Dense(y_sequence.shape[1], activation='softmax'))

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

print('Training started')
model.fit(X_train, y_train, epochs=10, batch_size=64, validation_split=0.1, callbacks=[TqdmCallback()])

print('Prediction started')
y_pred = model.predict(X_test)

# Evaluierung des Modells
accuracy = np.mean(np.argmax(y_pred, axis=1) == np.argmax(y_test, axis=1))
print(f'Average Accuracy: {accuracy * 100:.2f}%')

print("Vorhersagen:")
print(np.argmax(y_pred, axis=1))

# Auswertung des Modells
print(classification_report(y_test,y_pred))
