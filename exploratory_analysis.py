# -*- coding: utf-8 -*-
"""
Created on Wed May 22 21:14:10 2024

@author: wiesbrock
"""

import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
import scipy.stats as stats

column_names = ['Unnamed: 0','x1', 'x2', 'y1','y2','class', 'conf','frame','label']
data = pd.read_csv(r"C:\Users\wiesbrock\Desktop\Projekte\Foosball csv\concat\concatenated_file.csv")
names=data.columns

data=data[names[2:]]

x1=data['1']
x2=data['2']
y1=data['3']
y2=data['4']

label=data['8']

area=np.zeros((len(y2)))

for i in range(len(x1)):
    area[i]=(x2[i]-x1[i])*(y2[i]-y1[i])
    
plt.figure(dpi=300)
data=area[label==0],area[label==1],area[label==2],area[label==3],area[label==4],area[label==5],area[label==6],area[label==7]
sns.violinplot(data=data)

print(stats.f_oneway(area[label==0],area[label==1],area[label==2],area[label==3],area[label==4],area[label==5],area[label==6],area[label==7]))
print(stats.tukey_hsd(area[label==0],area[label==1],area[label==2],area[label==3],area[label==4],area[label==5],area[label==6],area[label==7]))

    
